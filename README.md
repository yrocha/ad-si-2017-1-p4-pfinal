# Projeto 4 - Projeto Final

Projeto Final da matéria Aplicações Distribuídas - 2017-1

Este projeto consiste no estudo e experimentação da arquitetura P2P, utilizando a tecnologia IPFS.

Neste estudo de caso, aproveitei para ensinar o passo-a-passo de como utilizar o IPFS.

![ipfs-splash](/uploads/fa788fa158f8be0decef80a18af4e4c5/ipfs-splash.png)

### Referências

* Todas as [Referências](https://gitlab.com/yrocha/ad-si-2017-1-p4-pfinal/wikis/referencias) podem ser consultadas [aqui](https://gitlab.com/yrocha/ad-si-2017-1-p4-pfinal/wikis/referencias)

### Fórum no Moodle

A orientação do projeto encontra-se no [Projeto 4](https://ead.inf.ufg.br/mod/forum/view.php?f=2975), disponível no Moodle.

## Documentação
Consulte a Documentação do Projeto, assim como o laboratório na [Wiki](https://gitlab.com/yrocha/ad-si-2017-1-p4-pfinal/wikis/home) 

## Changelogs

[Version 0.1](https://gitlab.com/yrocha/ad-si-2017-1-p4-pfinal/tags/Version-0.1)
* Introdução

[Version 1.0](https://gitlab.com/yrocha/ad-si-2017-1-p4-pfinal/tags/Version_v1.0)
* Finalização da Documentação
* Finalização do Laboratório